﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Business.Enum;

namespace Business {
    public partial class Module {


        [DbFunction("FORMATION2Model.store", "Statut_Session")]
        public static string Statut_Session(Int32 SES_id) {
            throw new NotSupportedException("Les appels direct sur cette fonction ne sont pas suportés");
        }

        [DbFunction("FORMATION2Model.store", "Statut_Cycle")]
        public static string Statut_Cycle(Int32 CYC_id) {
            throw new NotSupportedException("Les appels direct sur cette fonction ne sont pas suportés");
        }



        #region Module

        public eModuleDeliveryType GetDeliveryType() {
            switch (this.SES_typeSession) {
                case "CLASSE VIRTUELLE":
                    return eModuleDeliveryType.VIRTUAL_CLASSROOM;
                default:
                    return eModuleDeliveryType.ON_THE_SPOT;
            }
        }

        public bool IsActive() {
            if ((eStatus)Int32.Parse(Statut_Session((int)this.SES_id)) != eStatus.ANNULEE
                        && (eStatus)Int32.Parse(Statut_Session((int)this.SES_id)) != eStatus.REPORTEE)
                return true;
            return false;
        }

        public eStatus GetStatus() {
            return (eStatus)Int32.Parse(Statut_Session((int)this.SES_id));
        }

        #endregion

        #region Session

        public Session GetSession() {
            return (Session)this.CYC_SES_assembler.Select(x => x.CYC_cycle).FirstOrDefault();
        }

        #endregion

        #region Contributor

        /// <summary>
        /// Obtenir la liste de tous les intervenants sur la session.
        /// </summary>
        /// <returns>Null ou les intervenants</returns>
        public IQueryable<Contributor> GetContributor() {
            List<Contributor> result = new List<Contributor>();

            using (var db = new FORMATION2Entities())
            using (var dbItv = new INTERVEntities()) {
                foreach (var itv in db.spGetINTERVENANT_SESSION_With_Status((int)this.SES_id).AsQueryable()) {
                    result.Add(dbItv.Contributor.Where(x => x.CPT_INT == itv.CPT_INT).FirstOrDefault());
                }
            }
            return result == null ? default(IQueryable<Contributor>) : result.AsQueryable().Cast<Contributor>();
        }

        /// <summary>
        /// Obtenir la liste de tous les intervenants validé sur la session.
        /// </summary>
        /// <returns>Null ou les intervenants</returns>
        public IQueryable<Contributor> GetValidContributor() {
            List<Contributor> result = new List<Contributor>();

            using (var db = new FORMATION2Entities())
            using (var dbItv = new INTERVEntities()) {
                foreach (var itv in db.spGetINTERVENANT_SESSION_With_Status((int)this.SES_id).AsQueryable().Where(x => x.Statut.Equals("AFFECTE"))) {
                    result.Add(dbItv.Contributor.Where(x => x.CPT_INT == itv.CPT_INT).FirstOrDefault());
                }
            }
            return result == null ? default(IQueryable<Contributor>) : result.AsQueryable().Cast<Contributor>();
        }

        /// <summary>
        /// Obtenir la liste de tous les intervenants annulé sur la session.
        /// </summary>
        /// <returns>Null ou les intervenants</returns>
        public IQueryable<Contributor> GetCanceledContributor() {
            List<Contributor> result = new List<Contributor>();

            using (var db = new FORMATION2Entities())
            using (var dbItv = new INTERVEntities()) {
                foreach (var itv in db.spGetINTERVENANT_SESSION_With_Status((int)this.SES_id).AsQueryable().Where(x => x.Statut.Equals("DESISTE"))) {
                    result.Add(dbItv.Contributor.Where(x => x.CPT_INT == itv.CPT_INT).FirstOrDefault());
                }
            }
            return result == null ? default(IQueryable<Contributor>) : result.AsQueryable().Cast<Contributor>();
        }

        #endregion

        #region student

        public bool IsStudentRegistered(Student pax) {
            return GetStudent().Contains(pax);
        }

        public Double GetPresencePercent(Student pax) {
            Double duration = default(double);
            Double presence = default(double);

            foreach (var workShop in GetWorkShop()) {
                duration += workShop.GetDuration();
                presence += workShop.GetPresenceDuration(pax);
            }
            return (presence * 100) / duration;
        }


        public IQueryable<Student> GetStudent() {
            IQueryable<Student> result = default(IQueryable<Student>);

            foreach (var workShop in GetWorkShop()) {
                if (result == default(IQueryable<Student>))
                    result = workShop.GetStudent();
                else
                    result = result.Concat(workShop.GetStudent());
            }
            return result == default(IQueryable<Student>) ? result : result.Distinct();
        }

        #endregion

        #region WorkShop

        /// <summary>
        /// Retourne la liste des ateliers
        /// </summary>
        /// <returns>Null ou WorkShop</returns>
        public IQueryable<WorkShop> GetWorkShop() {
            return this.SPE_sessionPeriode.AsQueryable().Cast<WorkShop>();
        }

        /// <summary>
        /// Retourne la liste des ateliers validés
        /// </summary>
        /// <returns>Null ou WorkShop</returns>
        public IQueryable<WorkShop> GetValidWorkShop() {
            return this.SPE_sessionPeriode.AsQueryable().Where(x => x.SPE_etat != (int)eStatus.ANNULEE && x.SPE_etat != (int)eStatus.REPORTEE).Cast<WorkShop>();
        }

        /// <summary>
        /// Retourne la liste des ateliers annulés
        /// </summary>
        /// <returns>Null ou WorkShop</returns>
        public IQueryable<WorkShop> GetCanceledWorkShop() {
            return this.SPE_sessionPeriode.AsQueryable().Where(x => x.SPE_etat == (int)eStatus.ANNULEE || x.SPE_etat == (int)eStatus.REPORTEE).Cast<WorkShop>();
        }

        #endregion

    }
}
