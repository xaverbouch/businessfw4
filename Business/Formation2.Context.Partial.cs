﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Collections.Generic;

namespace Business {
    public partial class FORMATION2Entities : DbContext {

        private static readonly Dictionary<ObjectContext, FORMATION2Entities> contexts =
           new Dictionary<ObjectContext, FORMATION2Entities>();

        public static FORMATION2Entities FromObjectContext(ObjectContext context) {
            lock (contexts) {
                if (contexts.ContainsKey(context))
                    return contexts[context];
                return null; // context has propably been disposed
            }
        }

        public static FORMATION2Entities FromObject(object obj) {
            var field = obj.GetType().GetField("_entityWrapper");
            var wrapper = field.GetValue(obj);
            var property = wrapper.GetType().GetProperty("Context");
            var context = (ObjectContext)property.GetValue(wrapper, null);
            return FromObjectContext(context);
        }

        public FORMATION2Entities() {
            lock (contexts)
                contexts[((IObjectContextAdapter)this).ObjectContext] = this;
        }

        protected override void Dispose(bool disposing) {
            lock (contexts)
                contexts.Remove(((IObjectContextAdapter)this).ObjectContext);
            base.Dispose(disposing);
        }
    }
}
